#include QMK_KEYBOARD_H

// Custom keycodes
enum custom_keycodes {
    LAYR,
};

//  Tap Dance definitions
qk_tap_dance_action_t tap_dance_actions[] = {
    // TD(0) = On Tap -> MO(2); On hold -> MO(2); On double tap -> TO(3)
    [LAYR] = ACTION_TAP_DANCE_DOUBLE(MO(2), TO(0)),
};
     
// const uint16_t PROGMEM cmb_lctl[] = {MO(2), OSM(MOD_LCTL), COMBO_END};
// combo_t key_combos[COMBO_COUNT] = {
//     COMBO_ACTION(cmb_lctl),
//     // COMBO(cmb_lsft, KC_LSFT),
//     // COMBO(cmb_lgui, KC_LUI),
// };
// 
// void process_combo_event(uint16_t combo_index, bool pressed) {
//     switch (combo_index) {
//         case 0:
//             if (pressed) {
//                 register_code16(MOD_LCTL);
//             } else {
//                 unregister_code16(MOD_LCTL);
//             }
//             break;
//     }
// }

#include "../common_layout.c"
