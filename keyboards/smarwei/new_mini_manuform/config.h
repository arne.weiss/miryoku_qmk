// Copyright 2022 Arne Weiss (@smarwei)
// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once

#define TAPPING_TOGGLE 2

// Split Keyboard specific options
#define SPLIT_USB_DETECT
#define SOFT_SERIAL_PIN D2
//#define MASTER_RIGHT
//#define MASTER_LEFT

// Since we're aligning with the split_3x5_3 layout, ensure your matrix matches the 3x5 grid with 3 additional keys
// #define MATRIX_ROW_PINS { D7, E6, B4, B5 }
// #define MATRIX_COL_PINS { F6, F7, B1, B3, B2, B6 }
#define MATRIX_ROW_PINS_RIGHT { B1, B3, B2, B6 }
#define MATRIX_COL_PINS_RIGHT { B5, B4, E6, D7, C6, D4 }
